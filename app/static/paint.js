// Javascript file for paint tool
var canvas = document.getElementById("paint");
var ctx = canvas.getContext("2d");
var canvas_width = canvas.width;
var canvas_height =  canvas.height;
var curX, curY, prevX, prevY;
var hold = false;
ctx.lineWidth = 2;
var fill_value = true;
var stroke_value = false;
var canvas_data = {"brush": []}

function color(color_value){
    ctx.strokeStyle = color_value;
    ctx.fillStyle = color_value;
}

function add_pixel(){
    ctx.lineWidth += 1;
}

function reduce_pixel(){
    if (ctx.lineWidth == 1){
        ctx.lineWidth = 1;
    }
    else{
        ctx.lineWidth -= 1;
    }
}

function fill(){
    fill_value = true;
    stroke_value = false;
}

function outline(){
    fill_value = false;
    stroke_value = true;
}

function reset(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    canvas_data = {"brush": []}
}

function post_canvas_data (){
    var data = JSON.stringify(canvas_data);

    $.post("/", {canvas_data: data});
    console.log("canvas updated");
}

function brush () {
    console.log("In brush")
    canvas.onmousedown = function (e){
        curX = e.clientX - canvas.offsetLeft;
        curY = e.clientY - canvas.offsetTop;
        hold = true;

        prevX = curX;
        prevY = curY;
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
    };

    canvas.onmousemove = function (e){
        if(hold){
            curX = e.clientX - canvas.offsetLeft;
            curY = e.clientY - canvas.offsetTop;
            draw();
        }
    };

    canvas.onmouseup = function (e){
        end_draw();
    };


    canvas.onmouseout = function (e){
        end_draw();
    };

    function end_draw (){
        hold = false;
        post_canvas_data();
    }

    function draw (){
        ctx.lineTo(curX, curY);
        ctx.stroke();
        canvas_data.brush.push({ "startx": prevX, "starty": prevY, "endx": curX, "endy": curY,
            "thick": ctx.lineWidth, "color": ctx.strokeStyle });
    }
}




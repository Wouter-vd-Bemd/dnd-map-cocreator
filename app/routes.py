from flask import render_template, request
from app import app


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        canvas_width = 700
        canvas_height = 400
        return render_template('index.html', title="Home",
                               canvas_width=canvas_width, canvas_height=canvas_height)
    if request.method == 'POST':
        canvas_data = request.form['canvas_data']
        print(canvas_data)
        canvas_width = 700
        canvas_height = 400
        return render_template('index.html', title="Home",
                               canvas_width=canvas_width, canvas_height=canvas_height)
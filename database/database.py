import os
import sqlite3


class IdHandler:
    """Generator for unique IDs."""

    def __init__(self, start_value: int = -1):
        self._current_value = start_value

    def next(self) -> int:
        self._current_value += 1
        return self._current_value


class Database:
    """The main database class that is used for managing and creating the backend database."""

    connection: sqlite3.Connection = None
    cursor: sqlite3.Cursor = None

    task_id = None
    user_id = None
    board_id = None

    def __init__(self, database_file: str):
        self.create_connection(database_file)
        self.build_database_tables()

        self.task_id = IdHandler()
        self.user_id = IdHandler()
        self.board_id = IdHandler()

    @classmethod
    def build_database_tables(cls):
        """Constructs the database tables if they do not exist."""
        tasks_table = """
        CREATE TABLE IF NOT EXISTS tasks (
        task_id int PRIMARY KEY,
        user_id int NOT NULL,
        session_id int NOT NULL,
        timestamp datetime NOT NULL,
        action text NOT NULL
        )
        
        """

        user_info_table = """
        CREATE TABLE IF NOT EXISTS users (
        user_id int PRIMARY KEY
        )
        """

        session_info_table = """
        CREATE TABLE IF NOT EXISTS sessions (
        session_id int PRIMARY KEY,
        state text NOT NULL
        )
        """

        for create_table_command in [tasks_table, user_info_table, session_info_table]:
            try:
                cls.cursor.execute(create_table_command)
            except sqlite3.Error as e:
                print(e)

    @classmethod
    def close_connection(cls):
        """Closes the SQL database connection safely."""
        cls.connection.close()

    @classmethod
    def create_connection(cls, database_file: str):
        """Creates the basic SQL connection."""

        # Create folder if not present
        if not os.path.exists(database_file):
            os.mkdir(os.path.dirname(database_file))

        cls.connection = sqlite3.connect(database_file)
        cls.cursor = cls.connection.cursor()


if __name__ == "__main__":
    db_location = "../database/data/dnd-map-cocreator.db"
    Database(db_location)

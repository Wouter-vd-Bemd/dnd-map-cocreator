import datetime
from typing import List, Tuple

import numpy as np

import database
from session import Session


class Action:
    """Describes a single action a user can perform."""

    def __init__(self, session_id: int, user_id: int):
        self.task_id = database.sql_database.task_id.next()
        self.timestamp = datetime.datetime.now()
        self.user_id = user_id
        self.session_id = session_id

    def execute(self, session: Session):
        """Executes an action on the active session."""
        raise NotImplementedError("This action is not implemented yet.")


class CanvasAction(Action):
    """An action that is performed on the Canvas."""

    def __init__(self,
                 size: int,
                 color: Tuple[int, int, int],
                 coordinates: List[Tuple[int, int]],
                 session_id: int,
                 user_id: int):
        self.size = size
        self.color = color
        self.coordinates = coordinates
        super(CanvasAction, self).__init__(session_id, user_id)


class PaintAction(CanvasAction):
    """Painting action performed on the Canvas."""

    def _brush_on_coordinate(self, y_pos: int, x_pos: int, canvas: np.ndarray, color: np.ndarray):
        """Applies the brush on a coordinate."""
        y_start = max(y_pos - self.size, 0)
        y_end = min(y_pos + self.size, canvas.shape[0] - 1)

        x_start = max(x_pos - self.size, 0)
        x_end = min(x_pos + self.size, canvas.shape[0] - 1)

        for y_pixel in range(y_start, y_end):
            for x_pixel in range(x_start, x_end):
                if ((y_pixel - y_pos) ** 2 + (x_pixel - x_pos) ** 2) ** 0.5 < self.size:
                    canvas[y_pixel, x_pixel, :] = color

        return canvas

    def execute(self, session: Session):
        """Paints the Canvas."""
        color = np.array(self.color)

        for (y_pos, x_pos) in self.coordinates:
            session.canvas = self._brush_on_coordinate(y_pos, x_pos, session.canvas, color)


class EraserAction(PaintAction):
    """Eraser action performed on the Canvas."""

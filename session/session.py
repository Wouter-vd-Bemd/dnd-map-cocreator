import numpy as np


class Session:
    """Describes a session for one or more users."""

    def __init__(self, session_id: int):
        self.color_background = (255, 255, 255)
        self.session_id = session_id
        self.users = []
        self.canvas = np.ones((700, 900, 3))
        for color_index, value in enumerate(self.color_background):
            self.canvas[:, :, color_index] = value

# dnd-map-cocreator

## Getting Started
Run a flask development server by calling:
`flask run`

In order to run such that other pcs on you network have access, run:
`flask run --host=0.0.0.0`
WARNING: Only do this when you trust the network. Arbitrary python code
can be run through the flask server in this mode.

import unittest

from session import Session, actions


def create_session() -> Session:
    return Session(0)


class ActionTests(unittest.TestCase):

    def test_paint_action(self):
        """Tests the paint action."""
        action = actions.PaintAction(16, (0, 0, 0), [(0, 0), (1, 1)], 0, 0)
        session = create_session()
        action.execute(session)

    def test_eraser_action(self):
        """Tests the brush and eraser combination."""
        paint_action = actions.PaintAction(16,
                                           (0, 0, 0),
                                           [(xy, xy) for xy in range(50)],
                                           0,
                                           0)

        erase_action = actions.EraserAction(8,
                                            (255, 255, 255),
                                            [(xy, xy) for xy in range(10, 30)],
                                            0,
                                            0)

        session = create_session()
        paint_action.execute(session)
        erase_action.execute(session)


if __name__ == "__main__":
    unittest.main()
